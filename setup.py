#!/usr/bin/env python

from distutils.core import setup

setup(name='easy_inject',
        version='1.0',
        description='Easy to use dependency injection',
        author='Trevor Von Seggern',
        url='https://gitlab.com/tvo/py-dependency-injection',
        package_dir={"easy_inject": "src"},
        packages=['easy_inject'],
     )
