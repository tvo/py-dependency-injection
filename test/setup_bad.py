from __future__ import annotations


class Circular:
    def __init__(self, c: Circular):
        self.c = c


class CirTwoA:
    def __init__(self, other: CirTwoB):
        self.other = other


class CirTwoB:
    def __init__(self, other: CirTwoA):
        self.other = other
