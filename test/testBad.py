import unittest
from src import ServiceCollection
from .setup_bad import Circular
from .setup_bad import CirTwoA, CirTwoB

class TestDependencyInjection(unittest.TestCase):
    def test_SelfReference_Throws(self):
        # Given
        services = ServiceCollection()
        services.register(Circular)

        # When
        with self.assertRaises(Exception):
            # Then
            services.resolve(Circular)

    def test_loopOfTwo_throws(self):
        # Given
        services = ServiceCollection()
        services.register(CirTwoA)
        services.register(CirTwoB)

        # When
        with self.assertRaises(Exception):
            # Then
            services.resolve(CirTwoA)

if __name__ == '__main__':
    unittest.main()
