import unittest
from src import ServiceCollection
from test.setupGenericBaseClass import DependsOnGenericClass
from .setup_abstract import IA, IB, IC
from .setup_abstract import AClass, BClass, CClass, ABClass


class TestDependencyInjection(unittest.TestCase):
    def test_resolve_from_abstraction_chainOne(self):
        # Given
        services = ServiceCollection()
        services.register(AClass).As(IA)

        # When
        a = services.resolve(IA)

        # Then
        self.assertEqual(a.methodA(), 'A')

    def test_resolve_from_abstraction_chainTwo(self):
        # Given
        services = ServiceCollection()
        services.register(AClass).As(IA)
        services.register(BClass).As(IB)

        # When
        b = services.resolve(IB)

        # Then
        self.assertEqual(b.methodB(), 'AB')

    def test_resolve_from_abstraction_chainThree(self):
        # Given
        services = ServiceCollection()
        services.register(AClass).As(IA)
        services.register(BClass).As(IB)
        services.register(CClass).As(IC)

        # When
        c = services.resolve(IC)

        # Then
        self.assertEqual(c.methodC(), 'ABC')

    def test_two_abstractClasses_canResolveBoth(self):
        # Given
        services = ServiceCollection()
        services.register(ABClass).As(IA).As(IB)

        # When
        a = services.resolve(IA)
        b = services.resolve(IB)

        # Then
        self.assertEqual(a.methodA(), 'A')
        self.assertEqual(b.methodB(), 'B')

    def test_class_with_generic_base(self):
        # Given
        services = ServiceCollection()
        services.register(DependsOnGenericClass)

        # When
        a = services.resolve(DependsOnGenericClass)

        # Then
        self.assertEqual(a.method(), 'base')
        self.assertEqual(a.method2(), 'top')


if __name__ == '__main__':
    unittest.main()
