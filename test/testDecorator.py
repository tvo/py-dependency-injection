from __future__ import annotations
import unittest
from abc import ABC, abstractmethod
from src import ServiceCollection
from .setup_decorate import Interface, AClass, BClass, CClass
from .setup_decorate import AClassNeedy, BClassNeedy, CClassNeedy, Needs
from .setup_decorate import One, Two, Three
from .setup_decorate import InterfaceAlt, XClass, YClass, ZClass

class TestDependencyInjection(unittest.TestCase):
    def test_decorate(self):
        # Given
        services = ServiceCollection()
        services.register(AClass).As(Interface)
        services.decorate(BClass, Interface)

        # When
        i = services.resolve(Interface)

        # Then
        self.assertEqual(i.method(), 'AB')

    def test_decorate_twoLevels(self):
        # Given
        services = ServiceCollection()
        services.register(AClass).As(Interface)
        services.decorate(BClass, Interface)
        services.decorate(CClass, Interface)

        # When
        i = services.resolve(Interface)

        # Then
        self.assertEqual(i.method(), 'ABC')

    def test_decorate_canResolveAnotherService(self):
        # Given
        services = ServiceCollection()
        services.register(XClass)
        services.register(YClass).As(InterfaceAlt)
        services.decorate(ZClass, InterfaceAlt)

        # When
        i = services.resolve(InterfaceAlt)

        # Then
        self.assertEqual(i.method(), 'XYZ')

    def test_decorate_usesAnotherDecorator_fromBase(self):
        # Given
        services = ServiceCollection()
        services.register(AClass).As(Interface)
        services.decorate(BClass, Interface)
        services.decorate(CClass, Interface)
        services.register(Needs)
        services.register(AClassNeedy).As(InterfaceAlt)
        services.decorate(Two, InterfaceAlt)
        services.decorate(Three, InterfaceAlt)

        # When
        i = services.resolve(InterfaceAlt)

        # Then
        self.assertEqual(i.method(), 'a(ABC)23')

    def test_decorate_usesAnotherDecorator_fromMiddle(self):
        # Given
        services = ServiceCollection()
        services.register(AClass).As(Interface)
        services.decorate(BClass, Interface)
        services.decorate(CClass, Interface)
        services.register(Needs)
        services.register(One).As(InterfaceAlt)
        services.decorate(BClassNeedy, InterfaceAlt)
        services.decorate(Three, InterfaceAlt)

        # When
        i = services.resolve(InterfaceAlt)

        # Then
        self.assertEqual(i.method(), '1b(ABC)3')

    def test_decorate_usesAnotherDecorator_fromTop(self):
        # Given
        services = ServiceCollection()
        services.register(AClass).As(Interface)
        services.decorate(BClass, Interface)
        services.decorate(CClass, Interface)
        services.register(Needs)
        services.register(One).As(InterfaceAlt)
        services.decorate(Two, InterfaceAlt)
        services.decorate(CClassNeedy, InterfaceAlt)

        # When
        i = services.resolve(InterfaceAlt)

        # Then
        self.assertEqual(i.method(), '12c(ABC)')

    def test_decorate_withScope(self):
        # Given
        services = ServiceCollection()
        services.register(AClass).As(Interface).scoped()
        services.decorate(BClass, Interface)
        services.decorate(CClass, Interface)

        # when
        outside = services.resolve(Interface)
        with services.createScope() as scope:
            inAOne = scope.resolve(Interface)
            inATwo = scope.resolve(Interface)

            # Then
            self.assertEqual(inAOne, inATwo)
            self.assertNotEqual(outside, inAOne)

    def test_decorate_noService_errors(self):
        # Given
        services = ServiceCollection()

        # Then
        with self.assertRaises(Exception):
            services.decorate(BClass, Interface)

if __name__ == '__main__':
    unittest.main()
