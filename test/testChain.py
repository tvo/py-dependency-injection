import unittest
from src import ServiceCollection
from .setup_chain import AClass, BClass, CClass, ABClass

class TestDependencyInjection(unittest.TestCase):
    def test_ResisterService(self):
        # Given
        services = ServiceCollection()
        services.register(AClass)

        # When
        simple = services.resolve(AClass)

        # Then
        self.assertEqual(simple.method(), 'A')

    def test_GetsDependentService(self):
        # Given
        services = ServiceCollection()
        services.register(AClass)
        services.register(BClass)

        # When
        b = services.resolve(BClass)

        # Then
        self.assertEqual(b.method(), 'AB')

    def test_GetsDependentService_OrderDoesNotMatter(self):
        # Given
        services = ServiceCollection()
        services.register(BClass)
        services.register(AClass)

        # When
        b = services.resolve(BClass)

        # Then
        self.assertEqual(b.method(), 'AB')

    def test_ThreeChain(self):
        # Given
        services = ServiceCollection()
        services.register(AClass)
        services.register(BClass)
        services.register(CClass)

        # When
        c = services.resolve(CClass)

        # Then
        self.assertEqual(c.method(), 'ABC')

    def test_multiple_dependencies(self):
        # Given
        services = ServiceCollection()
        services.register(AClass) # -> None
        services.register(BClass) # -> A
        services.register(ABClass) # -> A, -> B -> A

        # When
        ab = services.resolve(ABClass)

        # Then
        self.assertEqual(ab.method(), 'AAB')

if __name__ == '__main__':
    unittest.main()
