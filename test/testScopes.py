from __future__ import annotations
import unittest
from src import ServiceCollection
from .setup_simple import SimpleClass
from .setupClasses import ConcreteClass, AbstractClass, AbstractClassTwo


class TestScopes(unittest.TestCase):
    def test_singleton_resolves_same_instance(self):
        # Given
        services = ServiceCollection()
        services.register(SimpleClass).singleton()

        # When
        a = services.resolve(SimpleClass)
        b = services.resolve(SimpleClass)

        # Then
        self.assertEqual(a, b)

    def test_transient_resolves_different_instances(self):
        # Given
        services = ServiceCollection()
        services.register(SimpleClass)

        # When
        a = services.resolve(SimpleClass)
        b = services.resolve(SimpleClass)

        # Then
        self.assertNotEqual(a, b)

    def test_scope_insideScope_sameInstance(self):
        # Given
        a = None
        b = None
        services = ServiceCollection()
        services.register(SimpleClass).scoped()

        # When
        with services.createScope():
            a = services.resolve(SimpleClass)
            b = services.resolve(SimpleClass)

        # Then
        self.assertEqual(a, b)

    def test_scope_outsideScope_not_sameInstance(self):
        # Given
        a = None
        b = None
        services = ServiceCollection()
        services.register(SimpleClass).scoped()

        # When
        with services.createScope() as scope:
            a = scope.resolve(SimpleClass)
        with services.createScope() as scope:
            b = scope.resolve(SimpleClass)

        # Then
        self.assertNotEqual(a, b)

    def test_scope_nested_callsTo_service(self):
        # Given
        a = b = c = None
        services = ServiceCollection()
        services.register(SimpleClass).scoped()

        # When
        with services.createScope() as scopeOne:
            a = scopeOne.resolve(SimpleClass)
            with services.createScope() as scopeTwo:
                b = scopeTwo.resolve(SimpleClass)
            c = scopeOne.resolve(SimpleClass)

        # Then
        self.assertNotEqual(a, b)
        self.assertEqual(a, c)

    def test_scope_nested_callsTo_scope(self):
        # Given
        a = b = c = None
        services = ServiceCollection()
        services.register(SimpleClass).scoped()

        # When
        with services.createScope() as scopeOne:
            a = scopeOne.resolve(SimpleClass)
            with scopeOne.createScope() as scopeTwo:
                b = scopeTwo.resolve(SimpleClass)
            c = scopeOne.resolve(SimpleClass)

        # Then
        self.assertNotEqual(a, b)
        self.assertEqual(a, c)

    def test_scopeThenTransient_lastWins(self):
        # Given
        services = ServiceCollection()
        services.register(SimpleClass).singleton().instancePerDependency()

        # When
        a = services.resolve(SimpleClass)
        b = services.resolve(SimpleClass)

        # Then
        self.assertNotEqual(a, b)

    def test_twoAbstractions_ScopeUpdatesBoth_ResolveSameInstance(self):
        # Given
        services = ServiceCollection()
        services.register(ConcreteClass).As(AbstractClass).As(AbstractClassTwo).scoped()

        # When
        a = services.resolve(AbstractClass)
        b = services.resolve(AbstractClassTwo)

        # Then
        self.assertEqual(a, b)

    def test_two_abstractClasses_hasSameInstance(self):
        # Given
        services = ServiceCollection()
        services.register(ConcreteClass).scoped().As(AbstractClass).As(AbstractClassTwo)

        # When
        a = services.resolve(AbstractClass)
        b = services.resolve(AbstractClassTwo)

        # Then
        self.assertEqual(a, b)

    def test_registerInstance(self):
        # Given
        services = ServiceCollection()
        instance = SimpleClass()
        services.registerInstance(instance)

        # When
        a = services.resolve(SimpleClass)

        # Then
        self.assertEqual(instance, a)


if __name__ == '__main__':
    unittest.main()
