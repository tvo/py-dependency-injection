from abc import ABC, abstractmethod


# c => b => c, each implement "Interface". Classic three level decorated class.
class Interface(ABC):
    @abstractmethod
    def method(self) -> str:
        pass


class AClass(Interface):
    def method(self) -> str:
        return 'A'


class BClass(Interface):
    def __init__(self, a: Interface):
        self.a = a

    def method(self) -> str:
        return self.a.method() + 'B'


class CClass(Interface):
    def __init__(self, b: Interface):
        self.b = b

    def method(self) -> str:
        return self.b.method() + 'C'


# z => y are InterfaceAlt, y needs x which is does not implement an interface
class InterfaceAlt(ABC):
    @abstractmethod
    def method(self) -> str:
        pass


class XClass:
    def method(self) -> str:
        return 'X'


class YClass(InterfaceAlt):
    def __init__(self, x: XClass):
        self.x = x

    def method(self) -> str:
        return self.x.method() + 'Y'


class ZClass(InterfaceAlt):
    def __init__(self, y: InterfaceAlt):
        self.y = y

    def method(self) -> str:
        return self.y.method() + 'Z'


class Needs:
    def __init__(self, n: Interface):
        self.n = n

    def method(self):
        return '(' + self.n.method() + ')'


# this set of classes references the needey class, which import the whole xyz decorated line.
class One(InterfaceAlt):
    def method(self) -> str:
        return '1'


class Two(InterfaceAlt):
    def __init__(self, a: InterfaceAlt):
        self.a = a

    def method(self) -> str:
        return self.a.method() + '2'


class Three(InterfaceAlt):
    def __init__(self, b: InterfaceAlt):
        self.b = b

    def method(self) -> str:
        return self.b.method() + '3'


class AClassNeedy(InterfaceAlt):
    def __init__(self, n: Needs):
        self.n = n

    def method(self) -> str:
        return 'a' + self.n.method()  # a(abc)


class BClassNeedy(InterfaceAlt):
    def __init__(self, a: InterfaceAlt, n: Needs):
        self.a = a
        self.n = n

    def method(self) -> str:
        return self.a.method() + 'b' + self.n.method()  # ab(abc)


class CClassNeedy(InterfaceAlt):
    def __init__(self, b: InterfaceAlt, n: Needs):
        self.b = b
        self.n = n

    def method(self) -> str:
        return self.b.method() + 'c' + self.n.method()  # abc(ABC)
