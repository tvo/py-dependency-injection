class AClass:
    def method(self) -> str:
        return 'A'


class BClass:
    def __init__(self, a: AClass):
        self.a = a

    def method(self) -> str:
        return self.a.method() + 'B'


class CClass:
    def __init__(self, b: BClass):
        self.b = b

    def method(self) -> str:
        return self.b.method() + 'C'


class ABClass:
    def __init__(self, a: AClass, b: BClass):
        self.a = a
        self.b = b

    def method(self) -> str:
        return self.a.method() + self.b.method()
