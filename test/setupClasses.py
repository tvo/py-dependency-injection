from __future__ import annotations
from abc import ABC, abstractmethod


class SimpleClass:
    def method(self) -> str:
        return 'SimpleClass'


class DeadSimpleClass:
    Dead = 'simple'


class DependsOnSimpleClass:
    def __init__(self, dep: SimpleClass):
        self.dep = dep

    def method(self) -> str:
        return self.dep.method() + ' also this'


class DepOnDead:
    def __init__(self, dep: DeadSimpleClass):
        self.dep = dep


class DepOnBothSimple:
    def __init__(self, dep: DeadSimpleClass, simple: SimpleClass):
        self.dep = dep
        self.simple = simple


class DependsOnSelf:
    def __init__(self, depSelf: DependsOnSelf):
        self.dep = depSelf


class AbstractClass(ABC):
    @abstractmethod
    def method(self):
        pass


class AbstractClassTwo(ABC):
    @abstractmethod
    def methodTwo(self):
        pass


class ConcreteClass(AbstractClass, AbstractClassTwo):
    def method(self):
        return 'concrete'

    def methodTwo(self):
        return 'concrete'


class AbstractClassDecorator(AbstractClass):
    def __init__(self, instance: AbstractClass):
        self.instance = instance

    def method(self):
        return 'decorative ' + self.instance.method()
