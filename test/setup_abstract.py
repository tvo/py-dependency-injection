from abc import ABC, abstractmethod


class IA(ABC):
    @abstractmethod
    def methodA(self) -> str:
        pass


class IB(ABC):
    @abstractmethod
    def methodB(self) -> str:
        pass


class IC(ABC):
    @abstractmethod
    def methodC(self) -> str:
        pass


class AClass(IA):
    def methodA(self) -> str:
        return 'A'


class BClass(IB):
    def __init__(self, a: IA):
        self.a = a

    def methodB(self) -> str:
        return self.a.methodA() + 'B'


class CClass(IC):
    def __init__(self, b: IB):
        self.b = b

    def methodC(self) -> str:
        return self.b.methodB() + 'C'


class ABClass(IA, IB):
    def methodA(self) -> str:
        return 'A'

    def methodB(self) -> str:
        return 'B'
