from __future__ import annotations
from typing import TypeVar, Generic

T = TypeVar("T")


class BaseClassWithGeneric(Generic[T]):
    def method(self) -> str:
        return 'base'


class DependsOnGenericClass(BaseClassWithGeneric[int]):
    def method2(self):
        return 'top'
