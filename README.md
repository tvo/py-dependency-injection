# Dependency Injection (python3)
This is a simple implementation for dependency injection.

A dependency injection library should:
* Di configuration lives outside the classes which are being configured. Ie no @injected for di hooks / registration.
* Handle class lifetimes (transient, scope, singleton)
* Support for the decorator pattern
* The service collection is not a singleton
* Be opensource!


# Install?
add this to your requirements file:
git+https://gitlab.com/tvo/py-dependency-injection.git#master@egg=easy_inject
