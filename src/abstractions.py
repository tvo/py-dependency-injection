from __future__ import annotations
from enum import Enum, auto
from abc import ABC, abstractmethod
from typing import Type, TypeVar

T = TypeVar("T")

class ServiceLifeTime(Enum):
    InstancePerDependency = auto()
    Scoped = auto()
    Singleton = auto()
    #another possible option for future, named scopes...


class IServiceDecorator(ABC):
    @abstractmethod
    def singleton(self) -> IServiceDecorator:
        pass

    @abstractmethod
    def scoped(self) -> IServiceDecorator:
        pass

    @abstractmethod
    def instancePerDependency(self) -> IServiceDecorator:
        pass

    def withLifeTime(self, life: ServiceLifeTime) -> IServiceDecorator:
        pass

    @abstractmethod
    def As(self, abscraction: Type[T]) -> IServiceDecorator:
        pass


class IScopeContext(ABC):
    @abstractmethod
    def __enter__(self) -> IScope:
        pass

    def __exit__(self, exc_type, exc_value, exc_traceback):
        pass


class IScope(ABC):
    @abstractmethod
    def createScope(self) -> IScopeContext:
        pass

    @abstractmethod
    def resolve(self, t: Type[T]) -> T:
        pass


class IServiceCollection(IScope):
    @abstractmethod
    def register(self, t: Type[T]) -> IServiceDecorator:
        pass

    @abstractmethod
    def decorate(self, decorator: Type[T], abscraction: Type[T]):
        pass
