from .abstractions import IScope, IScopeContext, IServiceCollection
from .ScopeContext import ScopeContext
from typing import Type, TypeVar

T = TypeVar("T")

class Scope(IScope):
    def __init__(self, services: IServiceCollection):
        self.instances = dict()
        self.services: IServiceCollection = services

    def _get_instance(self, name: str) -> T:
        return self.instances.get(name)

    def _store_instance(self, name: str, instance: T) -> T:
        self.instances[name] = instance
        return instance

    def createScope(self):
        # if I needed scope to be hiearchical, it would be here.
        return ScopeContext(Scope(self.services))

    def resolve(self, t: Type[T]) -> T:
        instance = self._get_instance(t)
        if instance != None:
            return instance
        return self.services.resolve(t, self)


