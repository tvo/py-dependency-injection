from dataclasses import replace
from typing import Type, TypeVar, Dict
from .abstractions import IServiceDecorator, ServiceLifeTime
from .ServiceRegistration import ServiceRegistration

T = TypeVar("T")

class ServiceDescriptor(IServiceDecorator):
    def __init__(self, reg: ServiceRegistration, regDict: Dict[str, ServiceRegistration]):
        self._life = reg.life
        self._concreteName = reg.name
        self._regs = []
        self._services = regDict

    def getConcreteReg(self):
        return self._services.get(self._concreteName)

    def getRegs(self):
        result = []
        if len(self._regs) == 0:
            result.append(self.getConcreteReg())
        else:
            for reg in self._regs:
                result.append(reg)
        return result

    def _removeSelf(self):
        if len(self._regs) == 0:
            del self._services[self._concreteName]

    def withLifeTime(self, lifeUpdate: ServiceLifeTime) -> IServiceDecorator:
        for reg in self.getRegs():
            reg = replace(reg, life=lifeUpdate)
            self._services[reg.name] = reg
        return self

    def singleton(self) -> IServiceDecorator:
        return self.withLifeTime(ServiceLifeTime.Singleton)

    def scoped(self) -> IServiceDecorator:
        return self.withLifeTime(ServiceLifeTime.Scoped)

    def instancePerDependency(self) -> IServiceDecorator:
        return self.withLifeTime(ServiceLifeTime.InstancePerDependency)

    def As(self, abstraction: Type[T]) -> IServiceDecorator:
        if len(self._regs) == 0:
            baseReg = self.getConcreteReg()
            self._removeSelf()
        else:
            baseReg = self._regs[0]
        reg = replace(baseReg, disguised=abstraction, name=abstraction.__name__)
        self._regs.append(reg)
        self._services[reg.name] = reg
        return self
