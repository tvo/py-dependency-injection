from .abstractions import IScope, IScopeContext

class ScopeContext(IScopeContext):
    def __init__(self, scope: IScope):
        self.scope = scope

    def __enter__(self):
        return self.scope

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.scope = None # don't allow this scope to be used again.


