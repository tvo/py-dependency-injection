from __future__ import annotations
import inspect
from typing import Type, TypeVar, Optional, Dict, Union, Callable
from dataclasses import replace, dataclass
from .abstractions import IServiceCollection, ServiceLifeTime, IServiceDecorator, IScopeContext, IScope
from .ServiceDescriptor import ServiceDescriptor
from .Scope import Scope
from .ServiceRegistration import ServiceRegistration

T = TypeVar("T")


class ServiceCollection(IServiceCollection):
    def __init__(self):
        self.instances = {}
        self.parentCollection: Optional[ServiceCollection] = None
        self.childCollection: Optional[ServiceCollection] = None
        self.services: Dict[str, ServiceRegistration] = {}
        self.scope = Scope(self)

    def createScope(self) -> IScopeContext:
        return self.scope.createScope()

    def register(self, t: Type[T]) -> IServiceDecorator:
        reg = ServiceRegistration(t.__name__, t, t)
        self.services[reg.name] = reg
        return ServiceDescriptor(reg, self.services)

    def registerInstance(self, instance: T) -> IServiceDecorator:
        reg = self.register(instance.__class__).singleton()
        self.scope._store_instance(instance.__class__.__name__, instance)
        return reg

    def _insertDecorator(self, decorated: ServiceRegistration):
        # save to services.
        ab_reg = self.services.get(decorated.name)
        self.services[decorated.name] = decorated

        if ab_reg is None:  # no conflict, nothing to pass to parent
            return

        if self.parentCollection is None:
            self.parentCollection = ServiceCollection()
            self.parentCollection.childCollection = self
        self.parentCollection._insertDecorator(ab_reg)

    def decorate(self, decorator: Type[T], abstraction: Type[T]):
        ab_reg = self.services.get(abstraction.__name__)
        if ab_reg is None:
            raise Exception('Cannot decorate a service which is not registered')
        dec_reg = replace(ab_reg, concrete=decorator)
        self._insertDecorator(dec_reg)

    def _get_service_reg(self, key: str) -> ServiceRegistration:
        return self.services.get(key)

    def _concrete_parameters(self, concrete: Type[T]) -> [(str, str)]:
        signature = inspect.signature(concrete)
        parameters = signature.parameters
        for param in parameters:
            type_name = parameters[param].annotation
            if hasattr(type_name, '__name__'):
                type_name = type_name.__name__
            if type_name == '_empty':
                continue
            yield param, type_name

    def _get_singleton_scope(self):
        if self.childCollection is not None:
            return self.childCollection._get_singleton_scope()
        return self.scope

    @dataclass
    class ResolveContext:
        t: Union[str, Type[T]]  # input can be Type, but t is set to a str after.
        reg: ServiceRegistration
        callerReg: ServiceRegistration  # used for decorators
        scope: IScope  # doesn't get changed between calls
        stack: [Type[T]]
        ctorMemo: Dict[str, Callable[[], Type[T]]]

    def _resolve_populate(self, context: ResolveContext, next_cb: Callable[[ResolveContext], T]) -> T:
        # name t a string type
        if hasattr(context.t, '__name__'):
            context = replace(context, t=context.t.__name__)
        # find reg value from t str
        if context.reg is None:
            context = replace(context, reg=self._get_service_reg(context.t))
        return next_cb(context)

    def _resolve_verticalMovement(self, context: ResolveContext, nextCb: Callable[[ResolveContext], T]) -> T:
        # down movement, can't find registration here, but the children could have it.
        if context.reg is None:
            if self.childCollection is None:
                raise Exception('Service not found: ', context.t)
            child_most = self.childCollection
            while child_most.childCollection is not None:
                child_most = child_most.childCollection
            return self.childCollection._resolve_all(context)
        # decorators traverse up
        elif context.callerReg is not None and context.callerReg.name == context.t:
            if self.parentCollection is not None:
                # replace the parent so it doesn't go all the way up
                return self.parentCollection._resolve_all(replace(context, reg=None, callerReg=None))
        return nextCb(context)

    def _resolve_scope(self, context: ResolveContext, nextCb: Callable[[ResolveContext], T]) -> T:
        isTrans = context.reg.life == ServiceLifeTime.InstancePerDependency
        isSingleton = context.reg.life == ServiceLifeTime.Singleton
        if isTrans:
            return nextCb(context)

        storeScope = self._get_singleton_scope() if isSingleton else context.scope
        key = context.reg.concrete.__name__
        instance = storeScope._get_instance(key) or storeScope._store_instance(key, nextCb(context))
        return instance

    def _resolve_stack(self, context: ResolveContext, nextCb: Callable[[ResolveContext], T]) -> T:
        if context.reg.concrete in context.stack:
            raise Exception('Circular dependency reached: ' + context.reg.name, context.stack)
        context.stack.append(context.reg.concrete.__name__)

        instance = nextCb(context)

        context.stack.pop()
        return instance

    def _resolve(self, context: ResolveContext) -> T:
        reg = context.reg
        ctorMemo = context.ctorMemo

        # quick resolve if already created.
        if reg.name in ctorMemo.keys():
            return ctorMemo[reg.name]()
        arguments = {}
        for (pName, typeName) in self._concrete_parameters(reg.concrete):
            arg = self._resolve_all(replace(context, t=typeName, callerReg=context.reg, reg=None))
            arguments[pName] = arg
        ctor = lambda: reg.concrete(**{**arguments})
        ctorMemo[reg.name] = ctor
        return ctor()

    def _resolve_all(self, context: ResolveContext) -> T:
        base = lambda c: self._resolve(c)
        scope = lambda c: self._resolve_scope(c, base)
        stack = lambda c: self._resolve_stack(c, scope)
        vertical = lambda c: self._resolve_verticalMovement(c, stack)
        populate = lambda c: self._resolve_populate(c, vertical)
        return populate(context)

    def resolve(self, t: Type[T], scope=None) -> T:
        scope = self.scope if scope is None else scope
        context = self.ResolveContext(t=t, reg=None, callerReg=None, scope=scope, ctorMemo={}, stack=[])
        return self._resolve_all(context)
