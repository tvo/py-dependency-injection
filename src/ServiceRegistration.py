from .abstractions import ServiceLifeTime
from typing import Type, TypeVar
from dataclasses import dataclass, field

T = TypeVar("T")

@dataclass
class ServiceRegistration:
    name: str
    concrete: Type[T]
    disguised: Type[T]
    life: ServiceLifeTime = field(default=ServiceLifeTime.InstancePerDependency)
